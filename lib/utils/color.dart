import 'package:flutter/material.dart';

class ColorsTheme {
  static const grey = Color.fromARGB(255, 193, 193, 193); //C1C1C1
  static const greyLight = Color.fromARGB(255, 204, 204, 204); //CCCCCC
  static const greyLight2 = Color.fromARGB(255, 235, 235, 235); //EBEBEB
  static const white = Color.fromARGB(255, 255, 255, 255); //#FFFF
  static const black = Color.fromARGB(255, 0, 0, 0); //#0000
  static const blackLight = Color.fromARGB(255, 74, 80, 85); //#4A5055
  static const blue = Color.fromARGB(255, 55, 181, 231); //#37B5E7
  static const blueLight = Color.fromARGB(255, 0, 170, 229); //00AAE5
  static const blueDark = Color.fromARGB(255, 0, 82, 116); //#005274
  static const red = Color.fromARGB(255, 220, 61, 53); //DC3D35
}
