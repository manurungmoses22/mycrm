import 'package:flutter/material.dart';

class TextStyles {
  static const fieldHeader =
      TextStyle(fontSize: 18, fontWeight: FontWeight.w600);
  static const homeHeader =
      TextStyle(fontSize: 19, fontWeight: FontWeight.w600);
  static const field20w600 =
      TextStyle(fontSize: 20, fontWeight: FontWeight.w600);
  static const fieldContent =
      TextStyle(fontSize: 16, fontWeight: FontWeight.w600);
  static const textStyle12w500 =
      TextStyle(fontSize: 13, fontWeight: FontWeight.w500);
}
