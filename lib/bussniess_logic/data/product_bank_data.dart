import 'package:mycrm/bussniess_logic/model/product_bank.dart';

List<ProductBank> productBank = [
  ProductBank(
      1,
      35,
      "Mandiri",
      "Bunga Spesial Tengaj Imlek 2023",
      "assets/image/mandiri.png",
      3,
      14,
      220,
      3.88,
      12,
      "1",
      "Ruko, Rukan, Rumah, Apartemen",
      "karyawan",
      1),
  ProductBank(
      2,
      15,
      "Panin",
      "Panin KPR & KPR XTRA",
      "assets/image/panin.png",
      1,
      9,
      220,
      9.90,
      10,
      "85%",
      "Ruko, Rukan, Rumah, Apartemen",
      "karyawan, pengusaha",
      1)
];
