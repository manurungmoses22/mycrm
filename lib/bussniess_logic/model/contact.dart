class Contact {
  final int id;
  final String name, gender, job, position, email, phone_num;
  Contact(
    this.id,
    this.name,
    this.gender,
    this.job,
    this.position,
    this.email,
    this.phone_num,
  );
}
