class Report {
  final int contact, product, loan, bank, apr_pinjaman, total_pinjaman;
  final String target, amount1, amount2;
  Report(this.contact, this.product, this.loan, this.bank, this.apr_pinjaman,
      this.total_pinjaman, this.target, this.amount1, this.amount2);
}
