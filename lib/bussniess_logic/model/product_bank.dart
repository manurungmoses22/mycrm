class ProductBank {
  final int id, rating, total_review, total_user, max_tenor, komisi, scale;
  final String name, product, image, loan_to_value, jaminan, target;
  final double fix_rate;

  ProductBank(
      this.id,
      this.scale,
      this.name,
      this.product,
      this.image,
      this.rating,
      this.total_review,
      this.total_user,
      this.fix_rate,
      this.max_tenor,
      this.loan_to_value,
      this.jaminan,
      this.target,
      this.komisi);
}
