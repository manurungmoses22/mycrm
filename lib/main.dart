import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mycrm/ui/pages/root_page.dart';
import 'package:mycrm/utils/color.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'MY CRM',
      theme: ThemeData(primarySwatch: Colors.blue, useMaterial3: true),
      home: RootPage(),
    );
  }
}
