import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mycrm/utils/color.dart';

class HeaderProduct extends StatelessWidget {
  const HeaderProduct({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 10.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            width: Get.width,
            height: Get.height * 0.04,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10)),
              border: Border.all(color: Colors.black),
              color: Colors.white,
            ),
            child: const Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Text(
                  "Cari berdasarkan nama",
                  style: TextStyle(color: ColorsTheme.grey),
                ),
                Icon(
                  Icons.search,
                  color: ColorsTheme.grey,
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10, bottom: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  width: Get.width * 0.35,
                  height: Get.height * 0.04,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    border: Border.all(color: ColorsTheme.grey),
                    color: ColorsTheme.blue,
                  ),
                  child: const Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Icon(
                        Icons.add,
                        color: ColorsTheme.white,
                      ),
                      Text(
                        "Tambah",
                        style: TextStyle(color: ColorsTheme.white),
                      ),
                    ],
                  ),
                ),
                Container(
                  width: Get.width * 0.35,
                  height: Get.height * 0.04,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    border: Border.all(color: ColorsTheme.grey),
                    color: ColorsTheme.white,
                  ),
                  child: const Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Icon(
                          Icons.sort,
                          color: ColorsTheme.black,
                        ),
                        Text(
                          "Sort",
                          style: TextStyle(color: ColorsTheme.black),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            width: Get.width,
            height: Get.height * 0.04,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10)),
              border: Border.all(color: Colors.black),
              color: Colors.white,
            ),
            child: const Padding(
              padding: EdgeInsets.only(left: 20, right: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "All",
                    style: TextStyle(
                        color: ColorsTheme.black, fontWeight: FontWeight.bold),
                  ),
                  Icon(
                    Icons.arrow_drop_down,
                    color: ColorsTheme.blue,
                  ),
                ],
              ),
            ),
          ),
          new Container()
        ],
      ),
    );
  }
}
