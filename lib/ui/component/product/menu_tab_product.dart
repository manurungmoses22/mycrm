import 'package:flutter/material.dart';
import 'package:mycrm/ui/component/contact/my_contact.dart';
import 'package:mycrm/ui/component/contact/other_contact.dart';
import 'package:mycrm/ui/component/product/bank_page.dart';
import 'package:mycrm/ui/pages/bank_page.dart';
import 'package:mycrm/utils/color.dart';

class MenuTabProduct extends StatefulWidget {
  const MenuTabProduct({Key? key}) : super(key: key);

  @override
  _MenuTabProductState createState() => _MenuTabProductState();
}

class _MenuTabProductState extends State<MenuTabProduct>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 2, vsync: this);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TabBar(
          indicatorColor: ColorsTheme.blueLight,
          labelColor: ColorsTheme.black,
          controller: _tabController,
          tabs: const [
            Tab(text: 'Bank'),
            Tab(text: 'Developer'),
          ],
        ),
        Expanded(
          child: TabBarView(
            controller: _tabController,
            children: const [BankPageList(), OtherContact()],
          ),
        ),
      ],
    );
  }
}
