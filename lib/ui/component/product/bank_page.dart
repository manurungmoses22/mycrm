import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mycrm/bussniess_logic/data/product_bank_data.dart';
import 'package:mycrm/ui/component/contact/header.dart';
import 'package:mycrm/ui/component/contact/list_contact.dart';
import 'package:mycrm/ui/component/product/header_product.dart';
import 'package:mycrm/utils/color.dart';

class BankPageList extends StatelessWidget {
  const BankPageList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 248, 245, 245),
      body: Padding(
        padding: EdgeInsets.all(20.0),
        child: Column(
          children: [
            HeaderProduct(),
            Expanded(
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: productBank.length,
                itemBuilder: (BuildContext context, int index) {
                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      width: MediaQuery.of(context).size.width * 0.8,
                      height: MediaQuery.of(context).size.height * 0.2,
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey,
                            offset: Offset(2, 6),
                            blurRadius: 1,
                            blurStyle: BlurStyle.normal,
                          ),
                        ],
                        color: Colors.white,
                      ),
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(10, 25, 10, 25),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image.asset(
                              productBank[index].image,
                              scale: productBank[index].scale.toDouble(),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(top: 20.0, bottom: 20),
                              child: Table(
                                columnWidths: const {
                                  0: FlexColumnWidth(1),
                                  1: FlexColumnWidth(1),
                                },
                                children: [
                                  TableRow(
                                    children: [
                                      Container(
                                        padding: const EdgeInsets.all(8),
                                        decoration: const BoxDecoration(
                                          border: Border(
                                            right: BorderSide(
                                              width: 1,
                                              color: Colors.grey,
                                            ),
                                          ),
                                        ),
                                        child: Column(
                                          children: [
                                            Text(
                                              "Rating:",
                                              style: TextStyle(
                                                  color: ColorsTheme.grey,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            _buildStarRating(
                                                productBank[index].rating),
                                            Text(
                                              "${productBank[index].total_review} Reviews",
                                              style: TextStyle(
                                                  color: ColorsTheme.black,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Container(
                                        padding: EdgeInsets.all(8),
                                        child: Column(
                                          children: [
                                            Text(
                                              "Users:",
                                              style: TextStyle(
                                                  color: ColorsTheme.grey,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            Text(
                                              productBank[index]
                                                  .total_user
                                                  .toString(),
                                              style: TextStyle(
                                                  color: ColorsTheme.black,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            Text(
                                              "Users yearly",
                                              style: TextStyle(
                                                  color: ColorsTheme.black,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  productBank[index].name,
                                  style: TextStyle(
                                      fontSize: 18,
                                      color: ColorsTheme.black,
                                      fontWeight: FontWeight.bold),
                                ),
                                Text(
                                  ' ${productBank[index].product}',
                                  style: TextStyle(
                                      fontSize: 18,
                                      color: ColorsTheme.blue,
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(top: 15, bottom: 15),
                              child: Table(
                                columnWidths: const {
                                  0: FlexColumnWidth(0.9),
                                  1: FlexColumnWidth(),
                                },
                                children: [
                                  TableRow(children: [
                                    Container(
                                      decoration: const BoxDecoration(
                                        border: Border(
                                          right: BorderSide(
                                            width: 1,
                                            color: Colors.white,
                                          ),
                                        ),
                                      ),
                                      child: Column(
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Row(
                                              children: [
                                                Icon(Icons
                                                    .insert_chart_outlined),
                                                Text(
                                                  'Fix Rate (Year): ${productBank[index].fix_rate}',
                                                  style:
                                                      TextStyle(fontSize: 12),
                                                ),
                                              ],
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Row(
                                              children: [
                                                Icon(Icons.access_time),
                                                Text(
                                                  'Max Tenor: ${productBank[index].max_tenor}',
                                                  style:
                                                      TextStyle(fontSize: 12),
                                                ),
                                              ],
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Row(
                                              children: [
                                                Icon(Icons.access_time),
                                                Text(
                                                  'Loan to Value: ${productBank[index].loan_to_value}',
                                                  style:
                                                      TextStyle(fontSize: 12),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Container(
                                      decoration: const BoxDecoration(
                                        border: Border(
                                          right: BorderSide(
                                            width: 1,
                                            color: Colors.white,
                                          ),
                                        ),
                                      ),
                                      child: Column(
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Row(
                                              children: [
                                                Icon(Icons.beenhere_outlined),
                                                Expanded(
                                                  child: Text(
                                                    'Jaminan: ${productBank[index].jaminan}',
                                                    style:
                                                        TextStyle(fontSize: 12),
                                                    maxLines: 3,
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Row(
                                              children: [
                                                Icon(Icons.person_2_outlined),
                                                Expanded(
                                                  child: Text(
                                                    'Target: ${productBank[index].target}',
                                                    style:
                                                        TextStyle(fontSize: 12),
                                                    maxLines: 2,
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Row(
                                              children: [
                                                Icon(Icons
                                                    .currency_exchange_outlined),
                                                Text(
                                                  'Komisi: ${productBank[index].komisi}',
                                                  style:
                                                      TextStyle(fontSize: 12),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ]),
                                ],
                              ),
                            ),
                            Container(
                              width: Get.width * 0.25,
                              height: Get.height * 0.04,
                              decoration: const BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10)),
                                  color: Colors.white,
                                  boxShadow: [
                                    BoxShadow(
                                        color: ColorsTheme.grey,
                                        offset: Offset(0, 0),
                                        blurRadius: 1,
                                        spreadRadius: 2)
                                  ]),
                              child: const Center(
                                  child: Text(
                                'Detail',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: ColorsTheme.blue,
                                    fontSize: 20),
                              )),
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildStarRating(int rating) {
    return Row(
      children: List.generate(5, (index) {
        if (index < rating.floor()) {
          return Icon(Icons.star, color: ColorsTheme.blue);
        } else {
          return Icon(Icons.star, color: ColorsTheme.grey);
        }
      }),
    );
  }
}
