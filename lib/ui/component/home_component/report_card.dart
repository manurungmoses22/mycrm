import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mycrm/bussniess_logic/data/report_data.dart';
import 'package:mycrm/utils/color.dart';
import 'package:mycrm/utils/textStyles.dart';

class ReportCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 20.0),
      child: Container(
        width: Get.width * 0.9,
        height: Get.height * 0.2,
        decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(Radius.circular(10)),
          border: Border.all(color: ColorsTheme.grey, width: 1),
          color: ColorsTheme.white,
        ),
        child: Center(
          child: Table(
            border: const TableBorder(
              verticalInside: BorderSide(color: ColorsTheme.grey),
              horizontalInside: BorderSide(color: ColorsTheme.grey),
            ),
            defaultColumnWidth: FixedColumnWidth(150.0),
            children: [
              TableRow(
                children: [
                  TableCell(
                    child: Container(
                      padding: EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Icon(Icons.person),
                          Column(
                            children: [
                              Text(
                                'Contact',
                                style: TextStyles.fieldHeader,
                              ),
                              Text(
                                report.contact.toString(),
                                style: TextStyles.fieldContent,
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  TableCell(
                    child: Container(
                      padding: EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Icon(Icons.description_outlined),
                          Column(
                            children: [
                              Text(
                                'Loan',
                                style: TextStyles.fieldHeader,
                              ),
                              Text(
                                report.loan.toString(),
                                style: TextStyles.fieldContent,
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              TableRow(
                children: [
                  TableCell(
                    child: Container(
                      padding: EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Icon(Icons.shopping_bag),
                          Column(
                            children: [
                              Text(
                                'Product',
                                style: TextStyles.fieldHeader,
                              ),
                              Text(
                                report.product.toString(),
                                style: TextStyles.fieldContent,
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  TableCell(
                    child: Container(
                      padding: EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Icon(Icons.credit_card),
                          Column(
                            children: [
                              Text(
                                'Bank',
                                style: TextStyles.fieldHeader,
                              ),
                              Text(
                                report.bank.toString(),
                                style: TextStyles.fieldContent,
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
