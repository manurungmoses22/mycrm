import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mycrm/bussniess_logic/data/profile_data.dart';
import 'package:mycrm/utils/color.dart';
import 'package:mycrm/utils/textStyles.dart';

class ProfileContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width * 0.9,
      height: Get.height * 0.2,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        border: Border.all(color: ColorsTheme.grey, width: 1),
        color: ColorsTheme.white,
      ),
      child: Padding(
        padding: const EdgeInsets.only(top: 20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Welcome Back,', style: TextStyles.field20w600),
            Text(
              '${profile.name} (${profile.allias})',
              style: TextStyles.fieldHeader,
            ),
            SizedBox(height: 16),
            Table(
              defaultVerticalAlignment: TableCellVerticalAlignment.middle,
              children: [
                TableRow(
                  children: [
                    TableCell(
                      child: Container(
                        padding: const EdgeInsets.symmetric(horizontal: 10.0),
                        decoration: const BoxDecoration(
                          border: Border(
                            right:
                                BorderSide(color: ColorsTheme.grey, width: 1.0),
                          ),
                        ),
                        child: Row(
                          children: [
                            Icon(
                              Icons.work_outline_outlined,
                              size: 15,
                            ),
                            SizedBox(width: 8),
                            Text(
                              profile.position,
                              style: TextStyles.textStyle12w500,
                            ),
                          ],
                        ),
                      ),
                    ),
                    TableCell(
                      child: Container(
                        padding: const EdgeInsets.symmetric(horizontal: 10.0),
                        child: Row(
                          children: [
                            Icon(
                              Icons.email_outlined,
                              size: 15,
                            ),
                            SizedBox(width: 8),
                            Text(
                              profile.email,
                              style: TextStyles.textStyle12w500,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                TableRow(
                  children: [
                    TableCell(
                      child: Container(
                        padding: const EdgeInsets.symmetric(horizontal: 10.0),
                        decoration: const BoxDecoration(
                          border: Border(
                            right:
                                BorderSide(color: ColorsTheme.grey, width: 1.0),
                          ),
                        ),
                        child: Row(
                          children: [
                            Text(
                              'ID:',
                              style: TextStyles.textStyle12w500,
                            ),
                            SizedBox(width: 8),
                            Text(
                              profile.id,
                              style: TextStyles.textStyle12w500,
                            ),
                          ],
                        ),
                      ),
                    ),
                    TableCell(
                      child: Container(
                        padding: const EdgeInsets.symmetric(horizontal: 10.0),
                        child: Row(
                          children: [
                            Icon(
                              Icons.phone,
                              size: 15,
                            ),
                            SizedBox(width: 8),
                            Text(
                              profile.phone_num,
                              style: TextStyles.textStyle12w500,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
