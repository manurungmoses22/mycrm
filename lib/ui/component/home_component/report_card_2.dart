import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:mycrm/bussniess_logic/data/report_data.dart';
import 'package:mycrm/utils/color.dart';
import 'package:mycrm/utils/textStyles.dart';

class ReportCard2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final formatCurrency = NumberFormat.currency(locale: 'id_ID', symbol: '');
    return Padding(
      padding: const EdgeInsets.only(top: 20.0),
      child: Container(
        width: Get.width * 0.9,
        height: Get.height * 0.2,
        decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(Radius.circular(10)),
          border: Border.all(color: ColorsTheme.grey, width: 1),
          color: ColorsTheme.white,
        ),
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                "Pinjaman Disetujui",
                style: TextStyles.fieldHeader,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    report.apr_pinjaman.toString(),
                    style: TextStyles.field20w600,
                  ),
                  Text(" / ${report.total_pinjaman}  Pinjaman")
                ],
              ),
              Divider(
                thickness: 1,
                indent: 10,
                endIndent: 10,
              ),
              Text(
                'Target',
                style: TextStyles.fieldHeader,
              ),
              Text(
                report.target,
                style: TextStyles.field20w600,
              ),
              Text(
                  'Rp ${formatCurrency.format(int.parse(report.amount1))} / ${formatCurrency.format(int.parse(report.amount2))}')
            ],
          ),
        ),
      ),
    );
  }
}
