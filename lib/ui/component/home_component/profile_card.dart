import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mycrm/ui/component/home_component/profile_content.dart';
import 'package:mycrm/utils/color.dart';

class ProfileCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: Get.height * 0.3,
      child: Stack(
        children: [
          Container(
            width: Get.width,
            height: Get.height * 0.2,
            child: Image.asset(
              'assets/image/loan_market2.png',
              fit: BoxFit.cover,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Icon(Icons.menu),
                Row(
                  children: [
                    Container(
                      height: Get.height * 0.035,
                      decoration: const BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.white,
                      ),
                      child: const CircleAvatar(
                        child: Icon(
                          Icons.notifications_outlined,
                          color: Colors.black,
                        ),
                        backgroundColor: Colors.transparent,
                      ),
                    ),
                    Container(
                      height: Get.height * 0.035,
                      decoration: const BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.white,
                      ),
                      child: const CircleAvatar(
                        child: Icon(
                          Icons.settings_outlined,
                          color: Colors.black,
                        ),
                        backgroundColor: Colors.transparent,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Positioned(
            top: Get.height * 0.12,
            left: Get.width * 0.05,
            right: Get.width * 0.05,
            bottom: 0,
            child: ProfileContent(),
          ),
          Positioned(
            top: Get.height * 0.04,
            left: Get.width * 0.4,
            right: Get.width * 0.4,
            bottom: Get.height * 0.1,
            child: CircleAvatar(
              radius: Get.width * 0.9,
              backgroundColor: ColorsTheme.grey,
              child: ClipOval(
                child: Image.asset(
                  'assets/image/profile.jpg',
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
