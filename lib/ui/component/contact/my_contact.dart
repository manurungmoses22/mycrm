import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mycrm/bussniess_logic/data/contact.dart';
import 'package:mycrm/ui/component/contact/header.dart';
import 'package:mycrm/ui/component/contact/list_contact.dart';
import 'package:mycrm/utils/color.dart';

class MyContatcPage extends StatelessWidget {
  const MyContatcPage({super.key});

  @override
  Widget build(BuildContext context) {
    return const Padding(
      padding: EdgeInsets.all(20.0),
      child: Column(
        children: [Header(), ListContact()],
      ),
    );
  }
}
