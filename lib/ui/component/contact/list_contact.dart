import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mycrm/bussniess_logic/data/contact.dart';
import 'package:mycrm/ui/pages/contact_detail.dart';

class ListContact extends StatelessWidget {
  const ListContact({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        for (var i in contact)
          GestureDetector(
            onTap: () {
              Get.to(ContactDetailPage(contactId: i.id));
            },
            child: Padding(
              padding: const EdgeInsets.only(top: 8.0, bottom: 8),
              child: Container(
                width: Get.width,
                height: Get.height * 0.1,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  border: Border.all(color: Colors.grey),
                  color: Colors.white,
                ),
                child: Center(
                  child: Row(
                    children: [
                      Container(
                        width: Get.width * 0.2,
                        height: Get.height * 0.06,
                        child: CircleAvatar(
                          child: Text(
                            getInitials(i.name),
                            style: TextStyle(color: Colors.white, fontSize: 20),
                          ),
                          backgroundColor: Colors.blue,
                        ),
                      ),
                      SizedBox(width: 10),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            i.name,
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Row(
                            children: [
                              (i.gender == "laki-laki")
                                  ? Icon(
                                      Icons.male,
                                      size: 20,
                                    )
                                  : Icon(
                                      Icons.female,
                                      size: 20,
                                    ),
                              Text(
                                i.gender,
                                style: TextStyle(fontSize: 12),
                              ),
                            ],
                          ),
                          Text(
                            i.job + " | " + i.position,
                            style: TextStyle(fontSize: 12),
                          ),
                          Text(
                            i.email,
                            style: TextStyle(fontSize: 12),
                          ),
                          Text(
                            i.phone_num,
                            style: TextStyle(fontSize: 12),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          )
      ],
    );
  }
}

String getInitials(String name) {
  List<String> nameWords = name.split(" ");
  String initials = "";

  for (String word in nameWords) {
    if (word.isNotEmpty) {
      initials += word.substring(0, 1);
    }
  }

  return initials;
}
