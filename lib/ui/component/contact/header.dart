import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mycrm/utils/color.dart';

class Header extends StatelessWidget {
  const Header({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            width: Get.width * 0.59,
            height: Get.height * 0.04,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10)),
              border: Border.all(color: Colors.black),
              color: Colors.white,
            ),
            child: const Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Text(
                  "Cari berdasarkan nama",
                  style: TextStyle(color: ColorsTheme.grey),
                ),
                Icon(
                  Icons.search,
                  color: ColorsTheme.grey,
                ),
              ],
            ),
          ),
          Container(
            width: Get.width * 0.23,
            height: Get.height * 0.04,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10)),
              border: Border.all(color: Colors.black),
              color: ColorsTheme.blue,
            ),
            child: const Center(
              child: Text(
                "Tambah",
                style: TextStyle(color: ColorsTheme.white),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
