import 'package:flutter/material.dart';
import 'package:mycrm/ui/component/contact/my_contact.dart';
import 'package:mycrm/ui/component/contact/other_contact.dart';
import 'package:mycrm/utils/color.dart';

class MenuTabContact extends StatefulWidget {
  const MenuTabContact({Key? key}) : super(key: key);

  @override
  _MenuTabContactState createState() => _MenuTabContactState();
}

class _MenuTabContactState extends State<MenuTabContact>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 2, vsync: this);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TabBar(
          indicatorColor: ColorsTheme.blueLight,
          labelColor: ColorsTheme.black,
          controller: _tabController,
          tabs: const [
            Tab(text: 'My Contact'),
            Tab(text: 'Other Contact'),
          ],
        ),
        Expanded(
          child: TabBarView(
            controller: _tabController,
            children: const [MyContatcPage(), OtherContact()],
          ),
        ),
      ],
    );
  }
}
