import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mycrm/bussniess_logic/data/contact_detail.dart';
import 'package:mycrm/bussniess_logic/model/contact.dart';
import 'package:mycrm/bussniess_logic/model/contact_detail.dart';
import 'package:mycrm/utils/color.dart';

class ContactDetailPage extends StatelessWidget {
  const ContactDetailPage({
    super.key,
    required this.contactId,
  });
  final int contactId;

  @override
  Widget build(BuildContext context) {
    ContactDetail contact = contactDetail.firstWhere((c) => c.id == contactId);
    return Scaffold(
        appBar: AppBar(
          backgroundColor: ColorsTheme.blueLight,
          leading: const BackButton(color: ColorsTheme.white),
          title: const Text(
            "Detail Contact",
            style: TextStyle(
                fontWeight: FontWeight.bold,
                color: ColorsTheme.white,
                fontSize: 25),
          ),
        ),
        body: ListView(children: [
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              children: [
                Text(
                  contact.name,
                  style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                ),
                Text(
                  "Date Created : ${contact.created_at}",
                  style: TextStyle(fontWeight: FontWeight.w500),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20.0),
                  child: Container(
                    decoration: BoxDecoration(
                      // borderRadius: const BorderRadius.all(Radius.circular(4)),
                      border: Border.all(color: Colors.grey),
                    ),
                    child: Column(
                      children: [
                        Container(
                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                            color: Colors.grey[200],
                            // borderRadius:
                            //     BorderRadius.vertical(top: Radius.circular(4)),
                          ),
                          child: const Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Contact',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 18),
                              ),
                              Row(
                                children: [
                                  Icon(
                                    Icons.edit,
                                    color: ColorsTheme.blueDark,
                                  ),
                                  Text(
                                    "Edit",
                                    style: TextStyle(
                                        color: ColorsTheme.blueDark,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18),
                                  )
                                ],
                              )
                            ],
                          ),
                        ),
                        Container(
                          decoration: const BoxDecoration(
                            border: Border(
                              top: BorderSide(color: Colors.grey),
                              bottom: BorderSide(color: Colors.grey),
                            ),
                          ),
                          child: Table(
                            columnWidths: const {
                              0: FixedColumnWidth(120),
                              1: FlexColumnWidth(),
                            },
                            border: const TableBorder(
                              verticalInside: BorderSide(color: Colors.grey),
                            ),
                            children: [
                              TableRow(
                                children: [
                                  const Padding(
                                    padding: EdgeInsets.all(6),
                                    child: Text(
                                      "Phone",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(6),
                                    child: Text(contact.phone_number),
                                  ),
                                ],
                              ),
                              TableRow(
                                children: [
                                  const Padding(
                                    padding: EdgeInsets.all(6),
                                    child: Text(
                                      "Email",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(6),
                                    child: Text(contact.email),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20, bottom: 20),
                  child: Container(
                    decoration: BoxDecoration(
                      // borderRadius: const BorderRadius.all(Radius.circular(4)),
                      border: Border.all(color: Colors.grey),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: Get.width,
                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                            color: Colors.grey[200],
                            // borderRadius:
                            //     BorderRadius.vertical(top: Radius.circular(4)),
                          ),
                          child: const Text(
                            'Personal Information',
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 18),
                          ),
                        ),
                        Container(
                          decoration: const BoxDecoration(
                            border: Border(
                              top: BorderSide(color: Colors.grey),
                              bottom: BorderSide(color: Colors.grey),
                            ),
                          ),
                          child: Table(
                            columnWidths: const {
                              0: FixedColumnWidth(120),
                              1: FlexColumnWidth(),
                            },
                            border: const TableBorder(
                              verticalInside: BorderSide(color: Colors.grey),
                            ),
                            children: [
                              TableRow(
                                children: [
                                  const Padding(
                                    padding: EdgeInsets.all(6),
                                    child: Text(
                                      "Type Contact",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(6),
                                    child: Text(contact.type_contact),
                                  ),
                                ],
                              ),
                              TableRow(
                                children: [
                                  const Padding(
                                    padding: EdgeInsets.all(6),
                                    child: Text(
                                      "KTP",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(6),
                                    child: Text(contact.id_card),
                                  ),
                                ],
                              ),
                              TableRow(
                                children: [
                                  const Padding(
                                    padding: EdgeInsets.all(6),
                                    child: Text(
                                      "Birthday",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(6),
                                    child: Text(contact.birthday),
                                  ),
                                ],
                              ),
                              TableRow(
                                children: [
                                  const Padding(
                                    padding: EdgeInsets.all(6),
                                    child: Text(
                                      "Gender",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(6),
                                    child: Text(contact.gender),
                                  ),
                                ],
                              ),
                              TableRow(
                                children: [
                                  const Padding(
                                    padding: EdgeInsets.all(6),
                                    child: Text(
                                      "Adviser",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(6),
                                    child: Text(
                                        '${contact.advisor} (${contact.advisor_alias})'),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      height: Get.height * 0.05,
                      width: Get.width * 0.4,
                      decoration: const BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          color: ColorsTheme.blue),
                      child: const Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.share,
                            color: Colors.white,
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            "Share Access",
                            style: TextStyle(
                                fontSize: 15,
                                fontWeight: FontWeight.w600,
                                color: Colors.white),
                          )
                        ],
                      ),
                    ),
                    Container(
                      height: Get.height * 0.05,
                      width: Get.width * 0.4,
                      decoration: const BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          color: ColorsTheme.red),
                      child: const Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.delete,
                            color: Colors.white,
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            "Delete Access",
                            style: TextStyle(
                                fontSize: 15,
                                fontWeight: FontWeight.w600,
                                color: Colors.white),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20.0),
                  child: Container(
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      color: ColorsTheme.blueDark,
                    ),
                    child: const Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Application',
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w600,
                              fontSize: 18),
                        ),
                        Row(
                          children: [
                            Icon(
                              Icons.add_circle_rounded,
                              color: Colors.white,
                            ),
                            Text(
                              "Tambah",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w600,
                                  fontSize: 18),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: Container(
                    width: Get.width,
                    height: Get.height * 0.11,
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: ColorsTheme.grey,
                      ),
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    child: Row(
                      children: [
                        Container(
                          width: Get.width * 0.25,
                          decoration: BoxDecoration(
                            color: ColorsTheme.white,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          padding: const EdgeInsets.all(10),
                          child: Container(
                            decoration: BoxDecoration(
                              color: ColorsTheme.blueDark,
                              borderRadius: BorderRadius.circular(10),
                            ),
                            width: Get.width * 0.2,
                            child: const Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(
                                  Icons.abc,
                                  color: Colors.white,
                                  size: 30,
                                ),
                                Center(
                                  child: Text(
                                    "Kredit Pemilik Rumah Secondary (KPR)",
                                    maxLines: 3,
                                    overflow: TextOverflow.ellipsis,
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 8),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        const SizedBox(width: 10),
                        const Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "Nasabah",
                              style: TextStyle(fontWeight: FontWeight.w600),
                            ),
                            Text(
                              "Loan Amount",
                              style: TextStyle(fontWeight: FontWeight.w600),
                            ),
                            Text(
                              "Loan Step",
                              style: TextStyle(fontWeight: FontWeight.w600),
                            ),
                            Text(
                              "Tenor",
                              style: TextStyle(fontWeight: FontWeight.w600),
                            ),
                          ],
                        ),
                        const SizedBox(width: 10),
                        const Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              ":",
                              style: TextStyle(fontWeight: FontWeight.w600),
                            ),
                            Text(
                              ":",
                              style: TextStyle(fontWeight: FontWeight.w600),
                            ),
                            Text(
                              ":",
                              style: TextStyle(fontWeight: FontWeight.w600),
                            ),
                            Text(
                              ":",
                              style: TextStyle(fontWeight: FontWeight.w600),
                            ),
                          ],
                        ),
                        const SizedBox(width: 10),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(contact.nasabah),
                            Text(contact.loan_amount),
                            Text(contact.loan_step),
                            Text(contact.type_contact),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20.0),
                  child: Container(
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.grey),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                            color: Colors.grey[200],
                            // borderRadius:
                            //     BorderRadius.vertical(top: Radius.circular(4)),
                          ),
                          child: const Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Contact',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 18),
                              ),
                              Row(
                                children: [
                                  Icon(
                                    Icons.add_circle_rounded,
                                    color: ColorsTheme.black,
                                  ),
                                  Text(
                                    "Tambah",
                                    style: TextStyle(
                                        color: ColorsTheme.black,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18),
                                  )
                                ],
                              )
                            ],
                          ),
                        ),
                        Container(
                            decoration: const BoxDecoration(
                              border: Border(
                                top: BorderSide(color: Colors.grey),
                                bottom: BorderSide(color: Colors.grey),
                              ),
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Container(
                                    width: Get.width * 0.4,
                                    height: Get.height * 0.14,
                                    decoration: const BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10)),
                                        color: ColorsTheme.greyLight2),
                                    child: const Padding(
                                      padding: EdgeInsets.all(15.0),
                                      child: Column(
                                        children: [
                                          Text(
                                            "Note",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold),
                                          ),
                                          Text(
                                            "February 9, 2023",
                                            textAlign: TextAlign.start,
                                          ),
                                          Text(
                                            "Note",
                                            textAlign: TextAlign.start,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            )),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ]));
  }
}
