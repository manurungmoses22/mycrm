import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:mycrm/ui/pages/bank_page.dart';
import 'package:mycrm/ui/pages/contact_page.dart';
import 'package:mycrm/ui/pages/home.dart';
import 'package:mycrm/ui/pages/loan_page.dart';
import 'package:mycrm/ui/pages/product_page.dart';
import 'package:mycrm/utils/color.dart';

class RootPage extends StatelessWidget {
  const RootPage({super.key});

  @override
  Widget build(BuildContext context) {
    return GetX<GetTabController>(
      init: GetTabController(),
      builder: (tc) {
        var bottomBar = BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          backgroundColor: ColorsTheme.white,
          unselectedItemColor: ColorsTheme.black,
          unselectedLabelStyle: const TextStyle(color: ColorsTheme.black),
          showUnselectedLabels: true,
          selectedItemColor: ColorsTheme.blue,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Home'),
            BottomNavigationBarItem(icon: Icon(Icons.person), label: 'Contact'),
            BottomNavigationBarItem(
                icon: Icon(Icons.description_outlined), label: 'Loan'),
            BottomNavigationBarItem(
                icon: Icon(Icons.shopping_bag), label: 'Product'),
            BottomNavigationBarItem(
                icon: Icon(Icons.credit_card), label: 'Bank'),
          ],
          currentIndex: tc.selectedIndex.value,
          onTap: tc.onItemTapped,
        );
        return SafeArea(
          child: Scaffold(
            body: Center(
              child: TabBarView(
                children: tc.widgetOptions,
                physics: NeverScrollableScrollPhysics(),
                controller: tc.tabController,
              ),
            ),
            bottomNavigationBar: bottomBar,
          ),
        );
      },
    );
  }
}

class GetTabController extends GetxController
    with GetSingleTickerProviderStateMixin {
  late TabController tabController;
  final RxInt selectedIndex = 0.obs;

  final List<Widget> widgetOptions = <Widget>[
    const HomePage(),
    const ContactPage(),
    const LoanPage(),
    const ProductPage(),
    const BankPage()
  ];

  void onItemTapped(int index) {
    tabController.animateTo(index);
    selectedIndex.value = index;
  }

  @override
  void onInit() {
    tabController = TabController(length: 5, vsync: this);
    super.onInit();
  }
}
