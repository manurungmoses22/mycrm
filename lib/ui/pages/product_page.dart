import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:mycrm/ui/component/contact/menu_tab.dart';
import 'package:mycrm/ui/component/product/menu_tab_product.dart';
import 'package:mycrm/utils/color.dart';

class ProductPage extends StatelessWidget {
  const ProductPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
          child: Container(
        height: Get.height * 0.89,
        child: Stack(
          children: [
            Container(
              width: Get.width,
              height: Get.height * 0.2,
              child: Image.asset(
                'assets/image/loan_market2.png',
                fit: BoxFit.cover,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Icon(Icons.menu),
                  Row(
                    children: [
                      Container(
                        height: Get.height * 0.035,
                        decoration: const BoxDecoration(
                          shape: BoxShape.circle,
                          color: Colors.white,
                        ),
                        child: const CircleAvatar(
                          child: Icon(
                            Icons.notifications_outlined,
                            color: Colors.black,
                          ),
                          backgroundColor: Colors.transparent,
                        ),
                      ),
                      Container(
                        height: Get.height * 0.035,
                        decoration: const BoxDecoration(
                          shape: BoxShape.circle,
                          color: Colors.white,
                        ),
                        child: const CircleAvatar(
                          child: Icon(
                            Icons.settings_outlined,
                            color: Colors.black,
                          ),
                          backgroundColor: Colors.transparent,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Positioned(
              top: Get.height * 0.12,
              bottom: 0,
              child: Container(
                  width: Get.width,
                  height: Get.height * 0.9,
                  decoration: BoxDecoration(
                    border: Border.all(color: ColorsTheme.grey, width: 1),
                    color: ColorsTheme.white,
                  ),
                  child: const MenuTabProduct()),
            ),
          ],
        ),
      )),
    );
  }
}
