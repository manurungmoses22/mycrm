import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:mycrm/bussniess_logic/data/profile_data.dart';
import 'package:mycrm/ui/component/home_component/profile_content.dart';
import 'package:mycrm/ui/component/home_component/profile_card.dart';
import 'package:mycrm/ui/component/home_component/report_card.dart';
import 'package:mycrm/ui/component/home_component/report_card_2.dart';
import 'package:mycrm/utils/color.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [ProfileCard(), ReportCard(), ReportCard2()],
      ),
    );
  }
}



// class HomePage extends StatelessWidget {
//   const HomePage({Key? key});

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: SingleChildScrollView(
//         child: Column(
//           children: [
//             Container(
//               height: Get.height * 0.3,
//               child: Stack(
//                 children: [
//                   Container(
//                     width: Get.width,
//                     height: Get.height * 0.2,
//                     child: Image.asset('assets/image/loan_market2.png',
//                         fit: BoxFit.cover),
//                   ),
//                   Padding(
//                     padding: const EdgeInsets.all(8.0),
//                     child: Row(
//                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                       children: [
//                         const Icon(Icons.label),
//                         Row(
//                           children: const [
//                             Icon(Icons.javascript),
//                             Icon(Icons.ad_units)
//                           ],
//                         )
//                       ],
//                     ),
//                   ),
//                   Positioned(
//                     top: Get.height * 0.12,
//                     left: Get.width * 0.05,
//                     right: Get.width * 0.05,
//                     bottom: 0,
//                     child: Container(
//                       width: Get.width * 0.9,
//                       height: Get.height * 0.2,
//                       decoration: BoxDecoration(
//                         borderRadius: BorderRadius.circular(10),
//                         border: Border.all(color: ColorsTheme.grey, width: 1),
//                         color: ColorsTheme.white,
//                       ),
//                       child: Padding(
//                         padding: const EdgeInsets.only(top: 20.0),
//                         child: Column(
//                           mainAxisAlignment: MainAxisAlignment.center,
//                           children: [
//                             Text('Welcome Back,'),
//                             Text('${profile.name} (${profile.allias})'),
//                             SizedBox(height: 16),
//                             Table(
//                               defaultVerticalAlignment:
//                                   TableCellVerticalAlignment.middle,
//                               children: [
//                                 TableRow(
//                                   children: [
//                                     TableCell(
//                                       child: Container(
//                                         padding: const EdgeInsets.symmetric(
//                                             horizontal: 10.0),
//                                         decoration: BoxDecoration(
//                                           border: Border(
//                                             right: BorderSide(
//                                                 color: ColorsTheme.grey,
//                                                 width: 1.0),
//                                           ),
//                                         ),
//                                         child: Row(
//                                           children: [
//                                             Icon(Icons.abc),
//                                             SizedBox(width: 8),
//                                             Text(profile.position),
//                                           ],
//                                         ),
//                                       ),
//                                     ),
//                                     TableCell(
//                                       child: Container(
//                                         padding: const EdgeInsets.symmetric(
//                                             horizontal: 10.0),
//                                         child: Row(
//                                           children: [
//                                             Icon(Icons.abc),
//                                             SizedBox(width: 8),
//                                             Text(profile.id),
//                                           ],
//                                         ),
//                                       ),
//                                     ),
//                                   ],
//                                 ),
//                                 TableRow(
//                                   children: [
//                                     TableCell(
//                                       child: Container(
//                                         padding: const EdgeInsets.symmetric(
//                                             horizontal: 10.0),
//                                         decoration: const BoxDecoration(
//                                           border: Border(
//                                             right: BorderSide(
//                                                 color: ColorsTheme.grey,
//                                                 width: 1.0),
//                                           ),
//                                         ),
//                                         child: Row(
//                                           children: [
//                                             Icon(Icons.abc),
//                                             SizedBox(width: 8),
//                                             Text(profile.email),
//                                           ],
//                                         ),
//                                       ),
//                                     ),
//                                     TableCell(
//                                       child: Container(
//                                         padding: const EdgeInsets.symmetric(
//                                             horizontal: 10.0),
//                                         child: Row(
//                                           children: [
//                                             Icon(Icons.abc),
//                                             SizedBox(width: 8),
//                                             Text(profile.phone_num),
//                                           ],
//                                         ),
//                                       ),
//                                     ),
//                                   ],
//                                 ),
//                               ],
//                             ),
//                           ],
//                         ),
//                       ),
//                     ),
//                   ),
//                   Positioned(
//                     top: Get.height * 0.04,
//                     left: Get.width * 0.4,
//                     right: Get.width * 0.4,
//                     bottom: Get.height * 0.1,
//                     child: CircleAvatar(
//                       radius: Get.width * 0.9,
//                       backgroundColor: ColorsTheme.grey,
//                       child: ClipOval(
//                         child: Image.asset(
//                           'assets/image/profile.jpg',
//                           fit: BoxFit.cover,
//                         ),
//                       ),
//                     ),
//                   ),
//                 ],
//               ),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }

// class HomePage extends StatelessWidget {
//   const HomePage({super.key});

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: SingleChildScrollView(
//         child: Column(
//           children: [
//             Container(
//               height: Get.height * 0.3,
//               child: Stack(
//                 children: [
//                   Container(
//                     width: Get.width,
//                     height: Get.height * 0.2,
//                     child: Image.asset('assets/image/loan_market2.png',
//                         fit: BoxFit.cover),
//                   ),
//                   Padding(
//                     padding: const EdgeInsets.all(8.0),
//                     child: Row(
//                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                       children: [
//                         const Icon(Icons.label),
//                         Row(
//                           children: const [
//                             Icon(Icons.javascript),
//                             Icon(Icons.ad_units)
//                           ],
//                         )
//                       ],
//                     ),
//                   ),
//                   Positioned(
//                     top: 101,
//                     left: 20,
//                     right: 20,
//                     bottom: 0,
//                     child: Container(
//                         width: Get.width * 0.9,
//                         height: Get.height * 0.2,
//                         decoration: BoxDecoration(
//                           borderRadius:
//                               const BorderRadius.all(Radius.circular(10)),
//                           border: Border.all(color: ColorsTheme.grey, width: 1),
//                           color: ColorsTheme.white,
//                         ),
//                         child: Column(
//                           children: [
//                             Text('Welcome Back,'),
//                             Text('${profile.name} (${profile.allias})'),
//                             Table(
//                               defaultVerticalAlignment:
//                                   TableCellVerticalAlignment.middle,
//                               children: [
//                                 TableRow(
//                                   children: [
//                                     TableCell(
//                                       child: Container(
//                                         padding: const EdgeInsets.symmetric(
//                                             vertical: 8.0),
//                                         decoration: const BoxDecoration(
//                                           border: Border(
//                                             right: BorderSide(
//                                                 color: ColorsTheme.grey,
//                                                 width: 1.0),
//                                           ),
//                                         ),
//                                         child: Row(children: [
//                                           Icon(Icons.abc),
//                                           Text(profile.position)
//                                         ]),
//                                       ),
//                                     ),
//                                     TableCell(
//                                       child: Container(
//                                         padding: const EdgeInsets.symmetric(
//                                             vertical: 8.0),
//                                         child: Row(children: [
//                                           Icon(Icons.abc),
//                                           Text(profile.id)
//                                         ]),
//                                       ),
//                                     ),
//                                   ],
//                                 ),
//                                 TableRow(
//                                   children: [
//                                     TableCell(
//                                       child: Container(
//                                         padding: const EdgeInsets.symmetric(
//                                             vertical: 8.0),
//                                         decoration: const BoxDecoration(
//                                           border: Border(
//                                             right: BorderSide(
//                                                 color: ColorsTheme.grey,
//                                                 width: 1.0),
//                                           ),
//                                         ),
//                                         child: Row(children: [
//                                           Icon(Icons.abc),
//                                           Text(profile.email)
//                                         ]),
//                                       ),
//                                     ),
//                                     TableCell(
//                                       child: Container(
//                                         padding: const EdgeInsets.symmetric(
//                                             vertical: 8.0),
//                                         child: Row(children: [
//                                           Icon(Icons.abc),
//                                           Text(profile.phone_num)
//                                         ]),
//                                       ),
//                                     ),
//                                   ],
//                                 ),
//                               ],
//                             ),
//                           ],
//                         )),
//                   ),
//                   Positioned(
//                     top: 65,
//                     left: 160,
//                     right: 160,
//                     bottom: 120,
//                     child: CircleAvatar(
//                       radius: Get.width * 0.05,
//                       backgroundColor: ColorsTheme.grey,
//                       child: ClipOval(
//                         child: Image.asset(
//                           'assets/image/profile.jpg',
//                           fit: BoxFit.cover,
//                         ),
//                       ),
//                     ),
//                   )
//                 ],
//               ),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }





